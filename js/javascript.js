// set variables and default values
const canvas = document.getElementById("cowField");
const context = canvas.getContext("2d");
const cow = new Image();

// starting coordinates and direction for cow
let x = 100;
let y = 100;
let z = 1;//1:East, 2:North, 3:West, 4:South

// display cow image on canvas
cow.src = "./images/cow.png";
cow.onload = () => {
    context.drawImage(cow, x, y);
};

function moveCow()
{
    var str = document.getElementById("instructions").value;
    const instructions = str.split("");
    instructions.forEach(move);
}

function move(movement)
{
    if (movement == "f") {
        forward();
    } else if (movement == "b") {
        backward();
    } else if (movement == "l") {
        left();
    } else if (movement == "r") {
        right();
    }
    clearCanvas();
    context.drawImage(cow, x, y);
}

// Forward 
function forward() {
    if (z == 1)
    {
        if (x < 400)
        {
            x += 10;
        } else
        {
            alert("Sorry! Cannot move cow as it will be out of field");
        }
    } else if (z == 2)
    {
        if (y > 10)
        {
            y -= 10;
        } else
        {
            alert("Sorry! Cannot move cow as it will be out of field");
        }
    } else if (z == 3)
    {
        if (x > 10) {
            x -= 10;
        } else
        {
            alert("Sorry! Cannot move cow as it will be out of field");
        }
    } else if (z == 4)
    {
        if (y < (600))
        {
            y += 10;
        } else
        {
            alert("Sorry! Cannot move cow as it will be out of field");
        }
    }
}

// Backward
function backward() {
    if (z == 1)
    {
        if (x > 10) {
            x -= 10;

        } else
        {
            alert("Sorry! Cannot move cow as it will be out of field");
        }
    } else if (z == 2)
    {
        if (y < (600))
        {
            y += 10;
        } else
        {
            alert("Sorry! Cannot move cow as it will be out of field");
        }

    } else if (z == 3)
    {
        if (x < 400)
        {
            x += 10;
        } else
        {
            alert("Sorry! Cannot move cow as it will be out of field");
        }

    } else if (z == 4)
    {
        if (y > 10)
        {
            y -= 10;
        } else
        {
            alert("Sorry! Cannot move cow as it will be out of field");
        }

    }
}

// Left Turn
function left() {
    if (z == 1)
    {
        z += 1;
        cow.src = "./images/cow2.png";
    } else if (z == 2)
    {
        z += 1;
        cow.src = "./images/cow3.png";
    } else if (z == 3)
    {
        z += 1;
        cow.src = "./images/cow4.png";
    } else
    {
        z = 1;
        cow.src = "./images/cow.png";
    }
}

// Right Turn
function right() {
    console.log("right: " + x + "," + y + "," + z);
    if (z == 2)
    {
        z -= 1;
        cow.src = "./images/cow.png";

    } else if (z == 3)
    {
        z -= 1;
        cow.src = "./images/cow2.png";

    } else if (z == 4)
    {
        z -= 1;
        cow.src = "./images/cow3.png";

    } else
    {
        z = 4;
        cow.src = "./images/cow4.png";
    }
}

//To Clear the Canvas
function clearCanvas() {
    canvas.width = canvas.width;
}
